angular.module('SignatureForm', [])
    .directive('signatureForm', ($parse) => {
        let link = (scope, element, attributes) => {
            let options       = angular.fromJson(attributes.options),
                signatureArea = angular.fromJson(attributes.signatureArea),
                image         = new Image();

            image.onload = () => {
                let model = $parse(attributes.ngModel);
                model.assign(scope, new Signature(image, options, signatureArea));
                model(scope).outlineSignatureArea();

                // Clear out canvases on reload
                if (element.find('canvas')) {
                    element.find('canvas').remove();
                }

                element.append(model(scope).canvas);
            };

            scope.$watch(
                () => $parse(attributes.imgSrc)(scope),
                (newSrc) => {
                    if (newSrc) {
                        image.src = newSrc;
                    }
                });
        };

        return {
            restrict: 'E',
            link: link
        }
    });
