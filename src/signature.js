'use strict';
class Signature {
    constructor(image, options, signatureArea = {}) {
        this.canvas        = document.createElement('canvas');
        this.mousePressed  = false;
        this._isSigned     = false;

        this.options       = Object.assign({
            lineWidth:   2,
            lineJoin:    'round',
            shadowBlur:  2,
            shadowColor: 'rgb(0, 0, 0)',
            height:      image.height,
            width:       image.width
        }, options);

        this.signatureArea = Object.assign({
            top:    image.height - 200,
            height: 200,
            left:   0,
            width:  image.width
        }, signatureArea);

        this.canvas.height = this.options.height;
        this.canvas.width  = this.options.width;

        this.canvas.addEventListener("mousedown",  this.mouseDown.bind(this));
        this.canvas.addEventListener("mouseup",    this.mouseUp.bind(this));
        this.canvas.addEventListener("mousemove",  this.mouseMove.bind(this));

        this.canvas.context             = this.canvas.getContext('2d');
        this.canvas.context.lineWidth   = this.options.lineWidth;
        this.canvas.context.lineJoin    = this.options.lineJoin;
        this.canvas.context.shadowBlur  = this.options.shadowBlur;
        this.canvas.context.shadowColor = this.options.shadowColor;

        this.canvas.context.drawImage(image, 0, 0, this.options.width, this.options.height);
    }

    outlineSignatureArea() {
        this.canvas.context.strokeRect(
            this.signatureArea.left,
            this.signatureArea.top,
            this.signatureArea.width,
            this.signatureArea.height);
    }

    getBase64(type = 'image/png') {
        return this.canvas.toDataURL(type)
    }

    getCoordinates(e) {
        this.consoleLog('clientX:'+e.clientX+', getBoundingClientRect().left:'+this.canvas.getBoundingClientRect().left+', clientY:'+e.clientY+', getBoundingClientRect().top:'+this.canvas.getBoundingClientRect().top);

        return {
            'x': e.clientX - this.canvas.getBoundingClientRect().left,
            'y': e.clientY - this.canvas.getBoundingClientRect().top
        };
    }

    isInSignatureArea(position) {
        return position.y > this.signatureArea.top &&
               position.x > this.signatureArea.left &&
               position.y < (this.signatureArea.top + this.signatureArea.height) &&
               position.x < (this.signatureArea.left + this.signatureArea.width);
    }

    get isSigned() {
        return this._isSigned;
    }

    consoleLog(msg) {
        if(this.options.debugMode) {
            console.log(msg);
        }
    }

    // Event listeners
    mouseDown(e) {
        let coordinates = this.getCoordinates(e);

        if (this.isInSignatureArea(coordinates)) {
            this.mousePressed   = true;

            this.consoleLog("beginPath()");
            this.canvas.context.beginPath();

            this.consoleLog("moveTo("+coordinates.x+", "+coordinates.y+")");
            this.canvas.context.moveTo(coordinates.x, coordinates.y);
        }
    }

    mouseUp() {
        this.mousePressed = false;

        this.consoleLog("closePath()");
        this.canvas.context.closePath();
    }

    mouseMove(e) {
        let coordinates = this.getCoordinates(e);

        if (this.mousePressed && this.isInSignatureArea(coordinates)) {
            this._isSigned = true;

            this.consoleLog("lineTo("+coordinates.x+", "+coordinates.y+")");
            this.canvas.context.lineTo(coordinates.x, coordinates.y);

            this.consoleLog("stroke()");
            this.canvas.context.stroke();
        }
    }

    clearSignature() {
        this.canvas.context.clearRect(this.signatureArea.left, this.signatureArea.top, this.signatureArea.width, this.signatureArea.height);
        this.outlineSignatureArea();
    }
}
