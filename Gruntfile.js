'use strict';
module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        /**
         * Creates a copy of signature.js which includes the custom Angular directive
         */
        concat: {
            dist: {
                src:  ['src/angular-wrapper.js', 'src/signature.js'],
                dest: 'bin/signature.angular.js'
            }
        },

        /**
         * Update the Javascript to ES5 standards.
         */
        babel: {
            options: {
                minified:  true,
                comments:  false,
                sourceMap: false,
                presets:   ['es2015']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd:    'src',
                    src:    ['signature.js'],
                    dest:   'bin'
                },{
                    src:  ['bin/signature.angular.js'],
                    dest: 'bin/signature.angular.js'
                }]
            }
        }
    });

    grunt.registerTask('default', ['concat', 'babel']);
};
